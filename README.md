# EC Simple
電商情境程式，教學用途
## API
- [swagger.ec.yml](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/e1cf4aac-05c6-4054-b028-f0f7c48c9465/swagger.ec.yml?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220718%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220718T121916Z&X-Amz-Expires=86400&X-Amz-Signature=fd8501baf1c5d6d58d01e3924f69cf2708762feffbc5c6eab547fd4e686c0183&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22swagger.ec.yml%22&x-id=GetObject)
## 啟動服務
進入專案資料夾，即可透過docker指令將服務啟動
```
sudo docker build -t ec-simple .
sudo docker run -p 7168:7168 ec-simple
```
## Postman
可以將 [/postman](/postman) 目錄下的 Collection 匯入至 postman 進行簡易測試，包含：
- [EC.postman_collection.json](postman/EC.postman_collection.json)
  - API的集合
- [EC.postman_environment.json](postman/EC.postman_environment.json)
  - API環境變數檔 (不匯入也可以，只有一個用於登入的 `token` 參數)