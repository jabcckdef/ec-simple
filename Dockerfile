FROM gradle:7.5.0-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM adoptopenjdk/openjdk11-openj9:latest
EXPOSE 7168
RUN mkdir /app

COPY --from=build /home/gradle/src/src/main/resources/*.yml /app/config/
COPY --from=build /home/gradle/src/build/libs/*.jar /app/ec-simple.jar

ENTRYPOINT ["java","-jar","/app/ec-simple.jar"]