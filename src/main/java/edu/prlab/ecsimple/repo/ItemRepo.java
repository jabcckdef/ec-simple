package edu.prlab.ecsimple.repo;

import edu.prlab.ecsimple.domain.LineItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepo extends JpaRepository<LineItem, String> {
}
