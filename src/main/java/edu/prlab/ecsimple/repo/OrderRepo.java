package edu.prlab.ecsimple.repo;

import edu.prlab.ecsimple.domain.TheOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo extends JpaRepository<TheOrder, Integer> {
}
