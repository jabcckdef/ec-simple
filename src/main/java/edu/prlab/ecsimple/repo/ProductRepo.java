package edu.prlab.ecsimple.repo;

import edu.prlab.ecsimple.domain.Product;
import edu.prlab.ecsimple.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepo extends JpaRepository<Product, String> {
}
