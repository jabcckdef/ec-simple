package edu.prlab.ecsimple.controller;

import edu.prlab.ecsimple.common.mapper.MapperUtil;
import edu.prlab.ecsimple.model.OrderModel;
import edu.prlab.ecsimple.model.ResponseModel;
import edu.prlab.ecsimple.service.OrderServiceImpl;
import edu.prlab.ecsimple.service.ProductService;
import edu.prlab.ecsimple.service.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ProductController {

    private final ProductServiceImpl productService;
    private final OrderServiceImpl orderService;


    @GetMapping("/product/{sku}")
    public ResponseEntity<ResponseModel> getProduct(@PathVariable String sku) {
        return productService.getProduct(sku)
                .map(MapperUtil::objectToJsonNode)
                .map(it -> ResponseEntity.ok()
                        .body(ResponseModel.builder()
                                .status("status")
                                .data(it)
                                .build()))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/product")
    public ResponseEntity<ResponseModel> getProductsByTags(@RequestBody List<String> tags) {
        return ResponseEntity.ok()
                .body(ResponseModel.builder()
                        .status("status")
                        .data(MapperUtil.objectToJsonNode(productService.getProductsByTags(tags)))
                        .build());
    }

    @GetMapping("/product")
    public ResponseEntity<ResponseModel> getProducts() {
        return ResponseEntity.ok()
                .body(ResponseModel.builder()
                        .status("status")
                        .data(MapperUtil.objectToJsonNode(productService.getProducts()))
                        .build());
    }

    @PostMapping("/checkout")
    public ResponseEntity<ResponseModel> checkout(@RequestBody OrderModel orderModel) {
        return orderService.checkout(orderModel)
                .map(MapperUtil::objectToJsonNode)
                .map(it -> ResponseEntity.ok()
                        .body(ResponseModel.builder()
                                .status("status")
                                .data(it)
                                .build()))
                .orElseGet(() -> ResponseEntity.badRequest().body(ResponseModel.builder()
                        .status("status")
                        .build()));
    }
}