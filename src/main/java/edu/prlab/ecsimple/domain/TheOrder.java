package edu.prlab.ecsimple.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
// Order在H2是保留字
public class TheOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer iid;

    @Builder.Default
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<LineItem> lineItems = new ArrayList<>();

    @Column
    private String email;

    @Column
    private String creditCard;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime checkoutDate;

    @Column
    private String state;

}
