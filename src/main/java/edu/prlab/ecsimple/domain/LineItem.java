package edu.prlab.ecsimple.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LineItem {

    @Id
    @Column(length = 36, nullable = false)
    private String sku;

    @Column
    private Integer quantity;
}
