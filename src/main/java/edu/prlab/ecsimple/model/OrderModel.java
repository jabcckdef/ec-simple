package edu.prlab.ecsimple.model;

import edu.prlab.ecsimple.domain.LineItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderModel {
    @Builder.Default
    private Collection<LineItem> lineItems = new ArrayList<>();
    private String email;
    private String creditCard;
}
