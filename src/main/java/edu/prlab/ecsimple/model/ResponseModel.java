package edu.prlab.ecsimple.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.configurationprocessor.json.JSONObject;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseModel {
    private String status;
    private JsonNode data;
}
