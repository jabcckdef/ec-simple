package edu.prlab.ecsimple.filter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class JwtConfig {

    private String url = "/api/login";

    private String header = "Authorization";

    private String prefix = "Bearer ";

    private long expiration = 24*60*60*1000;

    private String secret = "SecretKeyToGenJWTs";
}
