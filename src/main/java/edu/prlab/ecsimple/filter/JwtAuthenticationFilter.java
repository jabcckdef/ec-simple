package edu.prlab.ecsimple.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.prlab.ecsimple.domain.Account;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final JwtConfig config;

    @Override
    @SneakyThrows(IOException.class)
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        Account creds = new ObjectMapper().readValue(req.getInputStream(), Account.class);
        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(
                creds,
                creds.getPassword(),
                Collections.emptyList());
        setDetails(req, authReq);
        return authenticationManager.authenticate(authReq);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) throws IOException {
        Account account = (Account) auth.getPrincipal();
        String token = jwtTokenProvider.createToken(account.getUsername(), account.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));
        PrintWriter out = res.getWriter();
        res.setContentType("application/jwt");
        res.setCharacterEncoding("UTF-8");
        out.print(config.getPrefix() + token);
        out.flush();
    }
}
