package edu.prlab.ecsimple.config;

import edu.prlab.ecsimple.common.exception.EntityNotFoundException;
import edu.prlab.ecsimple.domain.*;
import edu.prlab.ecsimple.service.AccountServiceImpl;
import edu.prlab.ecsimple.service.OrderServiceImpl;
import edu.prlab.ecsimple.service.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.hibernate.id.GUIDGenerator;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class DefaultDataBean implements ApplicationRunner {
    private final AccountServiceImpl userService;
    private final ProductServiceImpl productService;
    private final OrderServiceImpl orderService;

    @Override
    public void run(ApplicationArguments args) {
        userService.create(Role.builder().name("ADMIN").build());
        userService.create(Role.builder().name("USER").build());
        userService.create(Account.builder().username("user1").password("1234").build());
        userService.addRoleToAccount("user1", "ADMIN");
        userService.addRoleToAccount("user1", "USER");
        productService.create(Product.builder()
                .name("product1")
                .tags(List.of("tag1", "tag2"))
                .quantity(10)
                .build());
        productService.create(Product.builder()
                .name("product2")
                .tags(List.of("tag2", "tag3"))
                .quantity(20)
                .build());
    }
}
