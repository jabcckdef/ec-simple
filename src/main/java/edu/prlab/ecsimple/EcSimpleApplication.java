package edu.prlab.ecsimple;

import edu.prlab.ecsimple.filter.JwtConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class EcSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcSimpleApplication.class, args);
    }

}
