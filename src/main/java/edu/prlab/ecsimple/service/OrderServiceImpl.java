package edu.prlab.ecsimple.service;

import edu.prlab.ecsimple.common.exception.BusinessException;
import edu.prlab.ecsimple.common.exception.EntityNotFoundException;
import edu.prlab.ecsimple.domain.LineItem;
import edu.prlab.ecsimple.domain.TheOrder;
import edu.prlab.ecsimple.model.OrderModel;
import edu.prlab.ecsimple.repo.ItemRepo;
import edu.prlab.ecsimple.repo.OrderRepo;
import edu.prlab.ecsimple.repo.ProductRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepo;
    private final ItemRepo itemRepo;

    private final ProductRepo productRepo;

    @Override
    public Optional<TheOrder> create(TheOrder theOrder) {
        return Optional.of(theOrder)
                .map(it -> orderRepo.save(theOrder));
    }

    @Override
    public Optional<LineItem> addItemFromProduct(LineItem lineItem) {
        return Optional.of(lineItem)
                .map(it -> productRepo.findById(it.getSku())
                        .map(product -> {
                            var leftQty = product.getQuantity() - lineItem.getQuantity();
                            if (leftQty < 0) {
                                throw new BusinessException("There is not enough stock with product: " + product.getSku());
                            }
                            product.setQuantity(leftQty);
                            return itemRepo.save(lineItem);
                        })
                        .orElseThrow(() -> new EntityNotFoundException("Not found Product: " + it.getSku())));
    }

    @Override
    public void addItemToOrder(Integer orderId, String sku) {
        orderRepo.findById(orderId)
                .map(order -> itemRepo.findById(sku)
                        .map(item -> order.getLineItems().add(item))
                        .orElseThrow(() -> new EntityNotFoundException("No found this order")))
                .orElseThrow(() -> new EntityNotFoundException("No found this order"));
    }


    @Override
    public Optional<TheOrder> checkout(OrderModel orderModel) {
        return Optional.of(orderModel)
                .map(it -> orderRepo.save(TheOrder.builder()
                        .email(orderModel.getEmail())
                        .creditCard(orderModel.getCreditCard())
                        .state("created")
                        .checkoutDate(LocalDateTime.now())
                        .build()))
                .map(order -> {
                    Collection<LineItem> lineItems = orderModel.getLineItems().stream()
                            .map(it -> addItemFromProduct(it)
                                    .orElseThrow(() -> new BusinessException("Save lineItem error")))
                            .peek(it -> addItemToOrder(order.getIid(), it.getSku()))
                            .collect(Collectors.toList());
                    order.setLineItems(lineItems);
                    return order;
                });
    }
}
