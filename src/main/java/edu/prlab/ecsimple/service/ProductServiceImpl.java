package edu.prlab.ecsimple.service;

import edu.prlab.ecsimple.domain.Product;
import edu.prlab.ecsimple.repo.ProductRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepo productRepo;

    @Override
    public Optional<Product> create(Product product) {
        return Optional.of(product).map(productRepo::save);
    }

    @Override
    public Optional<Product> getProduct(String sku) {
        return productRepo.findById(sku);
    }

    @Override
    public List<Product> getProductsByTags(List<String> tags) {
        return productRepo.findAll().stream()
                .filter(it -> it.getTags().stream().anyMatch(tags::contains))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> getProducts() {
        return productRepo.findAll();
    }
}