package edu.prlab.ecsimple.service;

import edu.prlab.ecsimple.domain.Role;
import edu.prlab.ecsimple.domain.Account;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    Optional<Account> create(Account account);
    Optional<Role> create(Role role);
    void addRoleToAccount(String username, String role);
    Optional<Account> getAccount(String username);
    List<Account> getAccounts();
    UserDetails loadUserByUsername(String username);
}
