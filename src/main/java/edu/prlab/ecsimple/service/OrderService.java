package edu.prlab.ecsimple.service;

import edu.prlab.ecsimple.domain.LineItem;
import edu.prlab.ecsimple.domain.TheOrder;
import edu.prlab.ecsimple.model.OrderModel;

import java.util.Optional;

public interface OrderService {
    Optional<TheOrder> create(TheOrder theOrder);
    Optional<LineItem> addItemFromProduct(LineItem lineItem);
    void addItemToOrder(Integer orderId, String sku);

    Optional<TheOrder> checkout(OrderModel orderModel);
}
