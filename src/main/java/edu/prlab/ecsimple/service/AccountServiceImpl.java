package edu.prlab.ecsimple.service;

import edu.prlab.ecsimple.common.exception.EntityNotFoundException;
import edu.prlab.ecsimple.domain.Account;
import edu.prlab.ecsimple.domain.Role;
import edu.prlab.ecsimple.repo.RoleRepo;
import edu.prlab.ecsimple.repo.AccountRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@RequiredArgsConstructor
@Service
@Slf4j
public class AccountServiceImpl implements AccountService, UserDetailsService {

    private final AccountRepo accountRepo;
    private final RoleRepo roleRepo;

    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public Optional<Account> create(Account user) {
        return Optional.of(user)
                .filter(it -> !accountRepo.existsByUsername(it.getUsername()))
                .map(it -> {
                    it.setPassword(passwordEncoder.encode(it.getPassword()));
                    return it;
                })
                .map(accountRepo::save);
    }

    @Override
    public Optional<Role> create(Role role) {
        return Optional.of(role)
                .map(roleRepo::save);
    }

    @Override
    public void addRoleToAccount(String username, String role) {
        accountRepo.findByUsername(username)
                .map(u -> roleRepo.findByName(role)
                        .map(r -> u.getRoles().add(r))
                        .orElseThrow(() -> new EntityNotFoundException("No found the role")))
                .orElseThrow(() -> new EntityNotFoundException("No found the User"));
    }

    @Override
    public Optional<Account> getAccount(String username) {
        return accountRepo.findByUsername(username);
    }

    @Override
    public List<Account> getAccounts() {
        return accountRepo.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return accountRepo.findByUsername(username).orElseThrow(() -> new EntityNotFoundException("No found the user"));
    }
}
