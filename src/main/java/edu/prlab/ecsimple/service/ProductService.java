package edu.prlab.ecsimple.service;

import edu.prlab.ecsimple.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Optional<Product> create(Product product);
    Optional<Product>  getProduct(String sku);

    List<Product> getProductsByTags(List<String> tags);
    List<Product> getProducts();
}
